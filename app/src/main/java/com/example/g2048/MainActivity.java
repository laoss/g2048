package com.example.g2048;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private Random rnd;
    private int[][] field;
    private int[][] saved;
    private Action[][] action;
    private boolean goon;
    private long score;
    private long prescore;
    private long maxscore;
    private final String maxScoreFileName = "maxScore.dat";

    private Animation alphaTransition;
    private Animation scaleTransition;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        alphaTransition = AnimationUtils.loadAnimation( this, R.anim.alpha );
        alphaTransition.reset();

        scaleTransition = AnimationUtils.loadAnimation( this, R.anim.scaler );
        scaleTransition.reset();


        findViewById( R.id.screen )
                .setOnTouchListener(
                        new OnSwipeTouchListener(this ){
                            public void onSwipeBottom() {
                                if( canMoveBottom() ) {
                                    saveState();
                                    moveBottom();
                                    proceedMoveResult();
                                } else {
                                    Toast.makeText(
                                            MainActivity.this,
                                            getString( R.string.no_move ),
                                            Toast.LENGTH_SHORT
                                    ).show();
                                }
                            }
                            public void onSwipeTop() {
                                if( canMoveTop() ) {
                                    saveState();
                                    moveTop();
                                    proceedMoveResult();
                                } else {
                                    Toast.makeText(
                                            MainActivity.this,
                                            getString( R.string.no_move ),
                                            Toast.LENGTH_SHORT
                                    ).show();
                                }
                            }
                            public void onSwipeLeft() {
                                if( canMoveLeft() ) {
                                    saveState();
                                    moveLeft();
                                    proceedMoveResult();
                                } else {
                                    Toast.makeText(
                                            MainActivity.this,
                                            getString( R.string.no_move ),
                                            Toast.LENGTH_SHORT
                                    ).show();
                                }
                            }
                            public void onSwipeRight() {
                                if( canMoveRight() ) {
                                    saveState();
                                    moveRight();
                                    proceedMoveResult();
                                } else {
                                    Toast.makeText(
                                            MainActivity.this,
                                            getString( R.string.no_move ),
                                            Toast.LENGTH_SHORT
                                    ).show();
                                }
                            }
                        } );

        findViewById( R.id.buttonBack )
                .setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick( View v ) {
                                restoreState();
                            }
                        }
                );

        findViewById( R.id.buttonRestart )
                .setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick( View v ) {
                                restart();
                            }
                        }
                );






        rnd = new Random();
        field = new int[4][4];
        saved = new int[4][4];
        action = new Action[4][4];
        restart();

    }

    private void saveMaxScore() throws IOException {
        FileOutputStream newScoreFile = openFileOutput( maxScoreFileName, Context.MODE_PRIVATE );
        DataOutputStream writer = new DataOutputStream( newScoreFile );
        writer.writeLong( maxscore );
        writer.close();
        newScoreFile.close();
    }

    private void restart() {
        goon = false;
        score = 0;
        try {
            FileInputStream maxScoreFile;
            maxScoreFile = openFileInput( maxScoreFileName );  // если приложение первый раз то тут будет эксепшн
            DataInputStream reader = new DataInputStream( maxScoreFile );
            maxscore = reader.readLong();
            reader.close();
            maxScoreFile.close();
        } catch( Exception ex ) {  // второй try для того что-бы в случае первого запуска приложения все равно сделать запись в файл
            try {
                saveMaxScore();
            } catch( Exception ex2 ) {
                maxscore = -1;
            }
        }

        clear();
        addVal();
        saveState();
        showField();
    }

    private TextView getCellByCoords(int x, int y) {
        int cellid = getResources()
                .getIdentifier(
                        "cell" + x + y,
                        "id",
                        getPackageName()
                );
        return findViewById( cellid );

    }

    private void clear() {
        int i,j;
        for( i = 0; i < 4; ++i ) {
            for( j = 0; j < 4; ++j ) {
                field[i][j] = 0;
                action[i][j] = Action.None;
            }
        }
    }

    private void saveState() {
        for( int i = 0; i < 4; ++i )
            for( int j = 0; j < 4; ++j )
                saved[i][j] = field[i][j];
    }

    private void restoreState() {
        for( int i = 0; i < 4; ++i )
            for( int j = 0; j < 4; ++j)
                field[i][j] = saved[i][j];
            score -= prescore;
            showField();

    }

    private void showField() {
        ( (TextView)
            findViewById( R.id.scoreTV )
        ).setText( score + "" );

        if( score > maxscore ) {
            maxscore = score;
            try {
                saveMaxScore();
            } catch( IOException ex ) {}
        }

        ( (TextView)
                findViewById( R.id.maxTV )
        ).setText( maxscore + "" );

        for( int i = 0; i < 4; ++i ) {
            for( int j = 0; j < 4; ++j ) {
                TextView cell = getCellByCoords( i, j );
                int bgColor, fgColor;
                switch( field[i][j] ) {
                    case 0 :
                        bgColor = 0xFFCCC0B4;
                        fgColor = 0xFFCCC0B4;
                        break;
                    case 2 :
                        bgColor = 0xFFEDE3D9;
                        fgColor = 0xFF736A61;
                        break;
                    case 4 :
                        bgColor = 0xFFECE0C8;
                        fgColor = 0xFF7A7067;
                        break;
                    case 8 :
                        bgColor = 0xFFF2B179;
                        fgColor = 0xFFFFFFFF;
                        break;
                    case 16 :
                        bgColor = 0xFFEB8E55;
                        fgColor = 0xFFFFFFFF;
                        break;
                    case 32 :
                        bgColor = 0xFFF57C5F;
                        fgColor = 0xFFFFFFFF;
                        break;
                    case 64 :
                        bgColor = 0xFFE85A34;
                        fgColor = 0xFFFFFFFF;
                        break;
                    case 128 :
                        bgColor = 0xFFF3D96B;
                        fgColor = 0xFFFFFFFF;
                        break;
                    case 256 :
                        bgColor = 0xFFE4C22D;
                        fgColor = 0xFFFFFFFF;
                        break;
                    case 512 :
                        bgColor = 0xFFE4C02A;
                        fgColor = 0xFFFFFFFF;
                        break;
                    case 1024 :
                        bgColor = 0xFFE4C22D;
                        fgColor = 0xFFFFFFFF;
                        break;
                    case 2048 :
                        bgColor = 0xFFE4C22D;
                        fgColor = 0xFFFFFFFF;
                        break;
                    case 4096 :
                        bgColor = 0xFFB784AB;
                        fgColor = 0xFFFFFFFF;
                        break;
                     default:
                         bgColor = 0xFFAA60A6;
                         fgColor = 0xFFFFFFFF;
                }
                cell.setBackgroundColor( bgColor );
                cell.setTextColor( fgColor );
                cell.setText( field[i][j] + "" );
                switch( action[i][j] ) {
                    case Appear:
                        cell.startAnimation( alphaTransition );
                        action[i][j] = Action.None;
                        break;
                    case Collapse:
                        cell.startAnimation( scaleTransition );
                        action[i][j] =Action.None;
                        break;
                }
            }
        }

    }

    private boolean addVal() {
        ArrayList<Coord> free = new ArrayList<>();
        for( int i = 0; i < 4; ++i ) {
            for( int j = 0; j < 4; ++j ) {
                if( field[i][j] == 0 )
                    free.add( new Coord( i, j ) );
            }
        }
        if( free.isEmpty() )
            return false;
        int r = rnd.nextInt( free.size() );
        field
                [ free.get(r).x ]
                [ free.get(r).y ] =
                    ( rnd.nextInt( 10 ) > 0 )
                            ? 2
                            : 4 ;
        action
             [free.get( r ).x ]
             [free.get( r ).y ] =
                 Action.Appear;


        free.remove( r );
        if( free.isEmpty() ) {
            // No free cells but may be move
            for( int i = 0; i < 4; ++i ) {
                for( int j = 0; j < 4; ++j ) {
                    if( i > 0 && field[i][j] == field[i-1][j] ) return true;
                    if( i > 3 && field[i][j] == field[i+1][j] ) return true;
                    if( j > 0 && field[i][j] == field[i][j-1] ) return true;
                    if( j > 0 && field[i][j] == field[i][j+1] ) return true;
                }
            }
            return false;
        }
        return true;
    }

    private boolean canMoveBottom() {
        int i, j, k;
        for( j = 0; j < 4; ++j ) {
            for( i = 3; i > 0; --i ) {
                if( field[i][j] == 0 ) {
                    k = i - 1;
                    while( k >= 0 && field[k][j] == 0 )
                        --k;
                    if( k >= 0 ) {
                        return true;
                    }
                }
            }
            for( i = 3; i > 0; --i ) {
                if( field[i][j] != 0 && field[i][j] == field[i-1][j] ) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean canMoveTop() {
        int i, j, k;
        for( j = 0; j < 4; ++j ) {
            for( i = 0; i < 3; ++i ) {
                if( field[i][j] == 0) {
                    k = i + 1;
                    while( k < 4 && field[k][j] == 0 )
                        ++k;
                    if( k < 4 ) {
                        return true;
                    }
                }
            }
            for( i = 0; i < 3; ++i ){
                if( field[i][j] != 0 && field[i][j] == field[i+1][j] ) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean canMoveLeft() {
        int i, j, k;
        for( i = 0; i < 4; ++i ) {
            for( j = 0; j < 3; ++j ) {
                if( field[i][j] == 0 ) {
                    k = j + 1;
                    while( k < 4 && field[i][k] == 0 )
                        ++k;
                    if( k < 4 ) {
                        return true;
                    }
                }
            }

            for( j = 0; j < 3; ++j ) {
                if( field[i][j] != 0 && field[i][j] == field[i][j + 1] ) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean canMoveRight() {
        int i, j, k;
        for( i = 0; i < 4; ++i ) {
            for( j = 3; j > 0; --j ) {
                if( field[i][j] == 0 ) {
                    k = j - 1;
                    while( k >= 0 && field[i][k] == 0 )
                        --k;
                    if( k >= 0 ) {
                        return true;
                    }
                }
            }

            for( j = 3; j > 0; --j ) {
                if( field[i][j] != 0 && field[i][j] == field[i][j - 1] ) {
                    return true;
                }
            }
        }
        return false;
    }

    private void moveBottom() {
        int i, j, k;
        for( j = 0; j < 4; ++j ) {
            for( i = 3; i > 0; --i ) {
                if( field[i][j] == 0 ) {
                    k = i - 1;
                    while( k >= 0 && field[k][j] == 0 )
                        --k;
                    if( k >= 0 ) {
                        field[i][j] = field[k][j];
                        field[k][j] = 0;
                    }
                }
            }


            for( i=3; i > 0; --i ) {
                if( field[i][j] != 0 && field[i][j] == field[i - 1][j] ) {
                    field[i][j] *= 2;
                    action[i][j] = Action.Collapse;
                    score += field[i][j];
                    prescore = field[i][j];
                    for( k = i - 1; k > 0; --k ) {
                        field[k][j] = field[k - 1][j];
                    }
                    field[0][j] = 0;
                }
            }
        }
    }

    private void moveTop() {
        int i, j, k;
        for( j = 0; j < 4; ++j ) {
            for( i = 0; i < 3; ++i ) {
                if( field[i][j] == 0 ) {
                    k = i + 1;
                    while( k < 4 && field[k][j] == 0 )
                        ++k;
                    if( k < 4 ) {
                        field[i][j] = field[k][j];
                        field[k][j] = 0;
                    }
                }
            }

            for( i = 0; i < 3; ++i ) {
                if(field[i][j] != 0 && field[i][j] == field[i + 1][j] ) {
                    field[i][j] *= 2;
                    action[i][j] = Action.Collapse;
                    score += field[i][j];
                    prescore = field[i][j];
                    for( k = i + 1; k < 3; ++k ) {
                        field[k][j] = field[k + 1][j];
                    }
                    field[3][j] = 0;
                }
            }
        }
    }

    private void moveLeft() {
        int i, j, k;
        for( i = 0; i < 4; ++i ) {
            for( j = 0; j < 3; ++j ) {
                if( field[i][j] == 0 ) {
                    k = j + 1;
                    while( k < 4 && field[i][k] == 0 )
                        ++k;
                    if( k < 4 ) {
                        field[i][j] = field[i][k];
                        field[i][k] = 0;
                    }
                }
            }
            for( j = 0; j < 3; ++j ) {
                if( field[i][j] != 0 && field[i][j] == field[i][j + 1] ) {
                    field[i][j] *= 2;
                    action[i][j] = Action.Collapse;
                    score += field[i][j];
                    prescore = field[i][j];
                    for( k = j + 1; k < 3; ++k ) {
                        field[i][k] = field[i][k + 1];
                    }
                    field[i][3] = 0;
                }
            }
        }

    }

    private void moveRight() {
        int i, j, k;
        for( i = 0; i < 4; ++i ) {
            for( j = 3; j > 0; --j ) {
                if( field[i][j] == 0 ) {
                    k = j - 1;
                    while( k >= 0 && field[i][k] == 0 )
                        --k;
                    if( k >= 0 ) {
                        field[i][j] = field[i][k];
                        field[i][k] = 0;
                    }
                }
            }
            for( j = 3; j > 0; --j ){
                if( field[i][j] != 0 && field[i][j] == field[i][j-1] ) {
                    field[i][j] *= 2;
                    action[i][j] = Action.Collapse;
                    score += field[i][j];
                    prescore = field[i][j];
                    for( k = j - 1; k > 0; --k ) {
                        field[i][k] = field[i][k-1];
                    }
                    field[i][0] = 0;
                }
            }
        }
    }

    private void epicFail() {
        new AlertDialog.Builder(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert )
                .setTitle( getString( R.string.fail ) )
                .setMessage( getString( R.string.play_again ) )
                .setIcon( android.R.drawable.ic_dialog_info )
                .setPositiveButton(R.string.once_more, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        restart();
                    }})
                .setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }})
                .setCancelable( false )
                .show();
    }

    private void proceedMoveResult() {
        if( ! goon ) {
            for( int i = 0; i < 4; ++i )
                for( int j = 0; j < 4; ++j)
                    if( field[i][j] == 2048 )
                        showWinMessage();

        }

        boolean res = addVal();
        showField();

        if( !res ) {
            epicFail();
        }
    }

    private void showWinMessage() {
        new AlertDialog.Builder(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert )
                .setTitle( getString( R.string.victory ) )
                .setMessage( getString( R.string.play_again) )
                .setIcon( android.R.drawable.ic_dialog_info )
                .setPositiveButton(R.string.once_more, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        restart();
                    }})
                .setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }})
                .setNeutralButton(R.string.goon, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        goon = true;
                    }})
                .setCancelable( false )
                .show();
    }

}






class Coord {
    public int x, y;
    public Coord( int i, int j ) { x = i; y = j; }
}

enum Action {
    None,
    Appear,
    Collapse
}
